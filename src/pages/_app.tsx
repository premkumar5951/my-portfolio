import "../styles/globals.css";
import { ChakraProvider } from "@chakra-ui/react";
import customTheme from "../styles/customTheme/index";
import React from "react";
import Head from "../../node_modules/next/head";

function MyApp({ Component, pageProps }) {
  return (
    <>
      
      <ChakraProvider theme={customTheme}>
        <Component {...pageProps} />
      </ChakraProvider>
    </>
  );
}

export default MyApp;
