import React from "react";
import {
  Container,
  HStack,
  Box,
  Text,
  Image,
  Link,
  VStack,
  Button,
  Badge,
  Divider
} from "@chakra-ui/react";
import Nav from "../../components/Nav";
import HomeLeft from "../../components/home/HomeLeft";
import HomeRight from "../../components/home/HomeRight";
import Skills from "../../components/home/Skills";
import Carausal from "../../components/home/Carausal";
import Carausal1 from "../../components/home/Carausal1";
import About from "../../components/home/About";
import Footer from "../../components/home/Footer";

const index = () => {
  return (
    <>
      <Container maxW="100%" p="0px">
        <Box w="100%" bg="#181E30">
          <Nav />
          <HStack
            w="full"
            mt="5em"
            p="5px"
            flexDirection={["column", "column", "row"]}
          > 
            <HomeLeft/>
            <HomeRight/>
          </HStack>
          <Divider width="90%" mx="auto" mt="5em" mb="1.3rem" color="#343A4E" opacity="0.1" height="2px"/>
          <About/>
          <Skills/>
          <Carausal1 title={"Internships & Experiences"}/>
          <Carausal title={"Personal Projects"}/>
          <Carausal title={"Certifications"}/>

          <Footer/>

        </Box>

      </Container>
    </>
  );
};

export default index;
