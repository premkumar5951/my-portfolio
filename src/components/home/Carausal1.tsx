import React from "react";
import {
  Container,
  HStack,
  Box,
  Text,
  Image,
  Link,
  VStack,
  Button,
  Badge,
  Icon,
} from "@chakra-ui/react";
import { BsCodeSquare } from "react-icons/bs";
import { HiArrowCircleLeft } from "react-icons/hi";
import { HiArrowCircleRight } from "react-icons/hi";
import { ArrowForwardIcon } from "@chakra-ui/icons";
import IconBtnMain from "../buttons/IconBtnMain";
import Slider from "react-slick";
import Head from "next/head";
import ViewAll from "../buttons/ViewAll";

function SampleNextArrow(props) {
  const { className, onClick } = props;
  return (
    <HStack
      w="80px"
      h="80px"
      pos="absolute"
      right="-4%"
      justifyContent="center"
      alignItems="center"
      borderRadius="10rem"
      color="black"
      shadow="0px 0px 20px -5px #625c5ca3"
      className={className}
      style={{ background: "#ffffff82" }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <HStack
      w="80px"
      h="80px"
      pos="absolute"
      left="-4%"
      justifyContent="center"
      alignItems="center"
      borderRadius="10rem"
      color="black"
      zIndex={100}
      shadow="0px 0px 20px -5px #625c5ca3"
      className={className}
      style={{ background: "#ffffff82" }}
      onClick={onClick}
    >
      <Icon as={HiArrowCircleLeft} fontSize="2em" color="white" />
    </HStack>
  );
}

const Carausal1 = ({ title }) => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrow: true,
    autoplay: true,
    autoplaySpeed: 3000,
    pauseOnHover: true,
    mobileFirst: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    appendDots: (dots) => (
      <ul style={{ margin: "0px" }} className="">
        {" "}
        {dots}{" "}
      </ul>
    ),
    customPaging: (i) => (
      <div
        style={{
          borderRadius: "10rem",
          width: "15px",
          height: "15px",
          color: "blue",
          background: "#ffffff82",
          margin: "2px",
          marginTop: "10px",
        }}
      />
    ),
  };
  return (
    <>
      <Head>
        <link rel="stylesheet" href="/css/animate.module.css" />
      </Head>
      <VStack
        w="80%"
        padding="1em"
        mx="auto"
        my="3rem"
        justify="center"
        alignItems="center"
      >
        <HStack w="full" px="0.5rem">
        <VStack w="full"  align="flex-start" justify="center" mb="3rem">
        <Text
          color="white"
          fontSize="1.8em"
          fontWeight="600"
          letterSpacing="0.7px"
          textAlign="left"
          w="full"
        >
          {title}
        </Text>
        <Box border="1px yellow solid" width="20%" />
        </VStack>
        <ViewAll/>
        </HStack>
        
        <div className="container" position="relative">
          <Slider {...settings}>
            {[1, 2, 3, 4, 5, 6, 7, 8, 9].map((item) => (
              <>
                <HStack
                  w="full"
                  h="full"
                  px="1rem"
                  cursor="pointer"
                  pos="relative"
                  box-shadow="0px 0px 20px -5px #ffffff7a"
                  className="animate-box"
                  overFlow="hidden"
                >
                  <Box flex="3" style={{ margin: "0px !important" }}>
                    <Image
                      border="2px #ffffff47 solid"
                      height="350px"
                      w="100%"
                      object="cover"
                      objectPosition="center"
                      borderLeftRadius="1em"
                      src="https://lh3.googleusercontent.com/p/AF1QipOSeHovGwYq76x0wy4HG97udXkA-J2Ijly7U-aF=s3490-w3490-h1774"
                    />
                  </Box>
                  <VStack
                    bg="white"
                    flex="4"
                    height="350px"
                    p="2em"
                    letterSpacing="0.7px"
                    alignItems="flex-start"
                    borderRightRadius="1em"
                    style={{ marginLeft: "0px !important" }}
                  >
                    <Text fontSize="1.5rem" fontWeight="600">
                      SBNRI Pvt. Ltd ~ New Delhi
                    </Text>
                    <HStack w="full" justifyContent="space-between">
                      <Text color="#0000009e">Role- Full Stack Developer</Text>
                      <Text fontWeight="600" color="#0000009e">JULY 2022 – PRESENT</Text>
                    </HStack>

                    <Text style={{"margin":"1.5rem 0px"}} color="#0000009e" fontWeight="500">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consectetur porro, dolores sit ab atque veniam molestiae maiores voluptates necessitatibus evenie</Text>

                    <IconBtnMain/>
                  </VStack>
                </HStack>
              </>
            ))}
          </Slider>
        </div>
      </VStack>
    </>
  );
};

export default Carausal1;
