import React from "react";
import {
  Container,
  HStack,
  Box,
  Text,
  Image,
  Link,
  VStack,
  Button,
  Badge,
  Icon,
} from "@chakra-ui/react";
import { BsCodeSquare } from "react-icons/bs";
import { FaDatabase } from "react-icons/fa";
import { FaTools } from "react-icons/fa";
import { TiTick } from "react-icons/ti";



import { ArrowForwardIcon } from "@chakra-ui/icons";
const Skills = () => {
  return (
    <>
      <VStack
        align="center"
        justify="center"
        mt="2em"
        w="80%"
        p="1em"
        mx="auto"
      >
        <VStack w="full"  align="flex-start" justify="center" mb="3rem">
        <Text
          color="white"
          fontSize="1.8em"
          fontWeight="600"
          letterSpacing="0.7px"
          textAlign="left"
          w="full"
        >
          Skills & Overview
        </Text>
        <Box border="1px yellow solid" width="10%" />
        </VStack>
        <HStack w="full" spacing="10" wrap="wrap">
          <VStack
            flex="1"
            h="245px"
            bg="white"
            position="relative"
            borderRadius="0.5rem"
            p="2rem"
            pt="3rem"
            shadow="0px 0px 20px -5px #ffffff70"
            outline="3px solid blueviolet"
          >
            <Box
              pos="absolute"
              textAlign="center"
              bg="#ffcc5e"
              width="5em"
              height="5em"
              borderRadius="5em"
              display="flex"
              justifyContent="center"
              alignItems="center"
              top="-35px"
              shadow="0px 0px 20px -5px #625c5ca3;"
            >
              <Icon as={BsCodeSquare} fontSize="2em" />
            </Box>
            <Text
              fontWeight="600"
              fontSize="1.2rem"
              letterSpacing="0.7px"
              pb="0.5rem"
            >
              Frontend
            </Text>
            <VStack
              textAlign="left"
              align="flex-start"
              w="full"
              fontWeight="500"
              fontSize="1rem"
              color="#0b0b0ba3"
              letterSpacing="0.7px"
            >
              <HStack alignItems="center">
                <Icon as={TiTick} fontSize="1.5em" color="#0084f7d9" />
                <Text>Reactjs/Nextjs/Redux</Text>
              </HStack>
              <HStack alignItems="center">
                <Icon as={TiTick} fontSize="1.5em" color="#0084f7d9" />
                <Text>Bootstrap/ChakraUi</Text>
              </HStack>
              <HStack alignItems="center">
                <Icon as={TiTick} fontSize="1.5em" color="#0084f7d9" />
                <Text>TainwindCss/Mui</Text>
              </HStack>
              <HStack alignItems="center">
                <Icon as={TiTick} fontSize="1.5em" color="#0084f7d9" />
                <Text>Js/Html/Css</Text>
              </HStack>
            </VStack>
          </VStack>
          <VStack
            flex="1"
            bg="white"
            h="245px"
            position="relative"
            borderRadius="0.5rem"
            p="2rem"
            pt="3rem"
            shadow="0px 0px 20px -5px #ffffff70"
            outline="3px solid blueviolet"
          >
            <Box
              pos="absolute"
              textAlign="center"
              bg="#ffcc5e"
              width="5em"
              height="5em"
              borderRadius="5em"
              display="flex"
              justifyContent="center"
              alignItems="center"
              top="-35px"
              shadow="0px 0px 20px -5px #625c5ca3;"
            >
              <Icon as={FaDatabase} fontSize="2em" />
            </Box>
            <Text
              fontWeight="600"
              fontSize="1.2rem"
              letterSpacing="0.7px"
              pb="0.5rem"
            >
              Backend
            </Text>
            <VStack
              textAlign="left"
              align="flex-start"
              w="full"
              fontWeight="500"
              fontSize="1rem"
              color="#0b0b0ba3"
              letterSpacing="0.7px"
            >
              <HStack alignItems="center">
                <Icon as={TiTick} fontSize="1.5em" color="#0084f7d9" />
                <Text>Python/Django</Text>
              </HStack>
              <HStack alignItems="center">
                <Icon as={TiTick} fontSize="1.5em" color="#0084f7d9" />
                <Text>Nodejs/Express</Text>
              </HStack>
            </VStack>
          </VStack>
          <VStack
            flex="1"
            h="245px"
            bg="white"
            position="relative"
            borderRadius="0.5rem"
            p="2rem"
            pt="3rem"
            shadow="0px 0px 20px -5px #ffffff70"
            outline="3px solid blueviolet"
          >
            <Box
              pos="absolute"
              textAlign="center"
              bg="#ffcc5e"
              width="5em"
              height="5em"
              borderRadius="5em"
              display="flex"
              justifyContent="center"
              alignItems="center"
              top="-35px"
              shadow="0px 0px 20px -5px #625c5ca3;"
            >
              <Icon as={FaTools} fontSize="2em" />
            </Box>
            <Text
              fontWeight="600"
              fontSize="1.2rem"
              letterSpacing="0.7px"
              pb="0.5rem"
            >
              Other Tools
            </Text>
            <VStack
              textAlign="left"
              align="flex-start"
              w="full"
              fontWeight="500"
              fontSize="1rem"
              color="#0b0b0ba3"
              letterSpacing="0.7px"

            >
              <HStack alignItems="center">
                <Icon as={TiTick} fontSize="1.5em" color="#0084f7d9" />
                <Text>Firebase/Aws</Text>
              </HStack>
              <HStack alignItems="center">
                <Icon as={TiTick} fontSize="1.5em" color="#0084f7d9" />
                <Text>Jira/Confluence</Text>
              </HStack>
              <HStack alignItems="center">
                <Icon as={TiTick} fontSize="1.5em" color="#0084f7d9" />
                <Text>Git/Docker</Text>
              </HStack>
              <HStack alignItems="center">
                <Icon as={TiTick} fontSize="1.5em" color="#0084f7d9" />
                <Text>Figma</Text>
              </HStack>
            </VStack>
          </VStack>
        </HStack>
      </VStack>
    </>
  );
};

export default Skills;
