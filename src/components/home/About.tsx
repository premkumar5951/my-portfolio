import React from "react";
import {
  Container,
  HStack,
  Box,
  Text,
  Image,
  Link,
  VStack,
  Button,
  Badge,
  Icon,
} from "@chakra-ui/react";
import { FaUniversity } from "react-icons/fa";
import { BsFillCalendarDateFill } from "react-icons/bs";
import { MdLocationPin } from "react-icons/md";
import { BsFillTelephoneFill } from "react-icons/bs";
import { MdEmail } from "react-icons/md";
import { FaDownload } from "react-icons/fa";

import { DiCodeigniter } from "react-icons/di";
import Social from "../Social";

const About = () => {
  return (
    <>
      <VStack
        align="center"
        justify="center"
        mt="2em"
        w="80%"
        p="1em"
        mx="auto"
      >
        <VStack w="full" align="flex-start" justify="center" mb="3rem">
          <Text
            color="white"
            fontSize="1.8em"
            fontWeight="600"
            letterSpacing="0.7px"
            textAlign="left"
            w="full"
          >
            About Me
          </Text>
          <Box border="1px yellow solid" width="5%" />
        </VStack>
        <HStack w="full" spacing="20" letterSpacing="0.7px">
          <VStack flex="3" align="flex-start">
            <Text
              textTransform="capitalize"
              color="#FECE2F"
              fontSize="1.5rem"
              fontWeight="600"
            >
              I'm a Fresher Full Stack Web Developer Having 1 Year of Internship
              Experience
            </Text>
            <VStack align="flex-start" style={{ margin: "1rem 0px" }}>
              <Text
                textTransform="capitalize"
                color="white"
                fontSize="1.2rem"
                fontWeight="600"
              >
                EDUCATION
              </Text>
              <Text
                color="#ffffffba"
                fontSize="1rem"
                fontWeight="500"
                textTransform="capitalize"
              >
                <Icon
                  as={FaUniversity}
                  color="#e15d5d"
                  width="1.2rem"
                  height="1.2rem"
                  mr="0.5em"
                />
                <span className="uppercase font-[600]">
                  JIS College of Engineering, Kalyani
                </span>{" "}
                — Bachelor of Technology (CGPA - 9.26*)
              </Text>
              <Text
                color="#ffffffba"
                fontSize="1rem"
                fontWeight="500"
                textTransform="capitalize"
              >
                <Icon
                  as={DiCodeigniter}
                  color="#e15d5d"
                  width="1.2rem"
                  height="1.2rem"
                  mr="0.5em"
                />
                <span className="uppercase font-[600]">Specialization</span> —
                Information Technology
              </Text>
            </VStack>
            <VStack align="flex-start" style={{ margin: "1rem 0px" }} w="full">
              <Text
                textTransform="capitalize"
                color="white"
                fontSize="1.2rem"
                fontWeight="600"
              >
                PERSONAL
              </Text>
              <HStack spacing="10" justify="space-between" w="full">
                <VStack flex="1" p="" align="flex-start">
                  <HStack w="full " align="center" justify="flex-start">
                    <Icon
                      as={MdLocationPin}
                      color="#e15d5d"
                      width="1.4rem"
                      height="1.4rem"
                      mr="0.5em"
                    />
                    <Text color="#ffffffba" fontSize="1rem" fontWeight="500">
                      <span>Patna-801110, Bihar</span>
                    </Text>
                  </HStack>
                  <HStack w="full " align="center" justify="flex-start">
                    <Icon
                      as={BsFillTelephoneFill}
                      color="#e15d5d"
                      width="1.2rem"
                      height="1.2rem"
                      mr="0.5em"
                    />
                    <Text color="#ffffffba" fontSize="1rem" fontWeight="500">
                      <span>+91 7903271879</span>
                    </Text>
                  </HStack>
                </VStack>
                <VStack flex="1" align="flex-end">
                  <HStack w="full " align="center" justify="flex-start">
                    <Icon
                      as={BsFillCalendarDateFill}
                      color="#e15d5d"
                      width="1.2rem"
                      height="1.2rem"
                      mr="0.5em"
                    />
                    <Text color="#ffffffba" fontSize="1rem" fontWeight="500">
                      <span>12th JAN, 2001</span>
                    </Text>
                  </HStack>
                  <HStack w="full " align="center" justify="flex-start">
                    <Icon
                      as={MdEmail}
                      color="#e15d5d"
                      width="1.4rem"
                      height="1.4rem"
                      mr="0.5em"
                    />
                    <Text color="#ffffffba" fontSize="1rem" fontWeight="500">
                      <span>premofdbapatna@gmail.com</span>
                    </Text>
                  </HStack>
                </VStack>
              </HStack>
              <HStack
                w="full"
                alignItems="center"
                justify="space-between"
                style={{ marginTop: "3rem" }}
              >
                <Social />
                <HStack flex="1" justify="flex-end">
                  <a
                    href="/Prem_resume.pdf"
                    download
                  >
                    <Button
                      fontSize="1.1em"
                      px="1em"
                      py="1.4em"
                      bg="#ffdb89"
                      color="#050505eb"
                      shadow="0px 0px 20px -5px #ffffff7a;"
                      _hover={{ bg: "#ffcd5d" }}
                      transition={"0.2s ease-in"}
                    >
                      <span>Download Resume</span>
                      <Icon
                        as={FaDownload}
                        color="#050505eb"
                        width="1.1rem"
                        height="1.1rem"
                        mx="0.5em"
                      />
                    </Button>
                  </a>
                </HStack>
              </HStack>
            </VStack>
          </VStack>
          <HStack flex="2">
            <Image
              src="https://firebasestorage.googleapis.com/v0/b/portfolio-595f6.appspot.com/o/Woman%20thinking%20about%20a%20work%20task.png?alt=media&token=b0fda34c-965a-4d16-bdde-db3f1b8c2bfa"
              objectFit="contain"
              className="animate-img-jump"
            />
          </HStack>
        </HStack>
      </VStack>
    </>
  );
};

export default About;
