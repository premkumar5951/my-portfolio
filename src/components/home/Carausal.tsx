import React from "react";
import {
  Container,
  HStack,
  Box,
  Text,
  Image,
  Link,
  VStack,
  Button,
  Badge,
  Icon,
} from "@chakra-ui/react";
import { BsCodeSquare } from "react-icons/bs";
import { HiArrowCircleLeft } from "react-icons/hi";
import { HiArrowCircleRight } from "react-icons/hi";
import Slider from "react-slick";
import Head from "../../../node_modules/next/head";
import ViewAll from "../buttons/ViewAll";

function SampleNextArrow(props) {
  const { className, onClick } = props;
  return (
    <HStack
      w="80px"
      h="80px"
      pos="absolute"
      right="-4%"
      justifyContent="center"
      alignItems="center"
      borderRadius="10rem"
      color="black"
      shadow="0px 0px 20px -5px #625c5ca3"
      className={className}
      style={{ background: "#ffffff82" }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <HStack
      w="80px"
      h="80px"
      pos="absolute"
      left="-4%"
      justifyContent="center"
      alignItems="center"
      borderRadius="10rem"
      color="black"
      zIndex={100}
      shadow="0px 0px 20px -5px #625c5ca3"
      className={className}
      style={{ background: "#ffffff82" }}
      onClick={onClick}
    >
        <Icon as={HiArrowCircleLeft} fontSize="2em" color="white"/>
    </HStack>
  );
}

const Carausal = ({title}) => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 2000,
    slidesToShow: 4,
    slidesToScroll: 4,
    arrow: true,
    autoplay: true,
    autoplaySpeed: 3000,
    pauseOnHover: true,
    mobileFirst: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    appendDots: dots => (
          <ul style={{ margin: "0px" }} className=""> {dots} </ul>

      ),
      customPaging: i => (
        <div
          style={{
            borderRadius:"10rem",
            width: "15px",
            height:"15px",
            color: "blue",
            background:"#ffffff82",
            margin:"2px",
            marginTop:"10px"
          }}
        />
      ),
    responsive: [
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
    ],
  };
  return (
    <>
      <Head>
        <link rel="stylesheet" href="/css/animate.module.css" />
      </Head>
      <VStack
        w="80%"
        padding="1em"
        mx="auto"
        // my="3rem"
        justify="center"
        alignItems="center"
      >
        <HStack w="full" px="0.5rem">
        <VStack w="full"  align="flex-start" justify="center" mb="3rem">
        <Text
          color="white"
          fontSize="1.8em"
          fontWeight="600"
          letterSpacing="0.7px"
          textAlign="left"
          w="full"
        >
        {title}

        </Text>
        <Box border="1px yellow solid" width="10%" />
        </VStack>
        <ViewAll/>
        </HStack>
        <div className="container" position="relative">
          <Slider {...settings}>
            {[1, 2, 3, 4, 5, 6, 7, 8, 9].map((item) => (
              <>
                <VStack
                  mx="0.5em"
                  cursor="pointer"
                  pos="relative"
                  className="animate-box"
                  borderRadius="1em"
                  overFlow="hidden"
                >
                  <HStack
                    borderRadius="1em"
                    bg="#00000080"
                    pos="absolute"
                    top="100%"
                    w="full"
                    h="full"
                    className="animate-inner"
                    justify="center"
                  >
                    <Button
                      bg="#ffdb89"
                      letterSpacing="0.7px"
                      fontSize="0.8em"
                      shadow="0px 0px 20px -5px #ffffff7a"
                    >
                      View detail
                    </Button>
                  </HStack>
                  <Box w="100%" style={{ margin: "0px !important" }}>
                    <Image
                      maxH="250px"
                      w="100%"
                      object="cover"
                      objectPosition="center"
                      borderTopRadius="1em"
                      src="https://wallpaperaccess.com/full/2026739.jpg"
                    />
                  </Box>
                  <VStack
                    bg="white"
                    w="full"
                    borderBottomRadius="1em"
                    p="1em"
                    style={{ margin: "0px !important" }}
                  >
                    <Text>Certificate</Text>
                  </VStack>
                </VStack>
              </>
            ))}
          </Slider>
        </div>
      </VStack>
    </>
  );
};

export default Carausal;
