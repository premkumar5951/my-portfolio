import React from 'react'
import {
    Container,
    HStack,
    Box,
    Text,
    Image,
    Link,
    VStack,
    Button,
    Badge,
  } from "@chakra-ui/react";
  import Typewriter from "typewriter-effect";
import { ArrowForwardIcon } from "@chakra-ui/icons";
const HomeRight = () => {
  return (
    <>
    <VStack p="1em" flex="1" align="flex-start" Justify="center" width="70%">
              <HStack spacing="5">
                <Box
                  height="1px"
                  width="3em"
                  bg="white"
                  border="1px solid white"
                ></Box>
                <Text
                  fontSize="1.5em"
                  color="white"
                  fontWeight="500"
                  letterSpacing="1px"
                >
                  Full Stack Developer
                </Text>
              </HStack>
              <Box
                fontSize="3em"
                color="#F7B843"
                fontWeight="600"
                letterSpacing="2px"
              >
                <Typewriter
                  options={{
                    strings: [
                      "Hey! I am @Prem Kumar",
                      "Welcome to my Portfolio.",
                      "I'm glad you are here ;)",
                    ],
                    autoStart: true,
                    loop: true,
                    delay: 100,
                  }}
                />
              </Box>
              <Box
                fontSize="1.3em"
                color="white"
                fontWeight="500"
                letterSpacing="1px"
                pt="0.7em"
                width="90%"
              >
                <Text color="#ffffff96" mb="3em">
                  A Passionate FullStack Web Developer having an experience of
                  building Web applications with{" "}
                  <Badge variant="solid" colorScheme="yellow" fontWeight="bold" opacity="1">
                    JavaScript
                  </Badge>{" "}
                  /{" "}
                  <Badge variant="solid" colorScheme="blue" fontWeight="bold" opacity="1">
                    Reactjs
                  </Badge>{" "}
                  /{" "}
                  <Badge variant="solid" colorScheme="yellow" fontWeight="bold" opacity="1">
                    Nodejs
                  </Badge>{" "}
                  /{" "}
                  <Badge variant="solid" colorScheme="red" fontWeight="bold" opacity="1">
                    Python
                  </Badge>{" "}
                  /{" "}
                  <Badge variant="solid" colorScheme="green" fontWeight="bold" opacity="1">
                    Django
                  </Badge>{" "}
                  and some other cool libraries and frameworks.
                </Text>

                {/* <Text w="full" textAlign="right" fontSize="0.5em" mt="3em">
                Yet ,It's the beginning, More to Learn & Explore ;)
                </Text> */}
              </Box>
              <Button
                fontSize="1.2em"
                px="1.2em"
                py="1.4em"
                bg="#ffdb89"
                color="#050505eb"
                shadow="0px 0px 20px -5px #ffffff7a;"
                _hover={{ bg: "white" }}
                transition={"0.2s ease-in"}
              >
                <span>View My Work</span>
                <ArrowForwardIcon mx="0.3em" fontSize="1.3em" />
              </Button>
            </VStack></>
  )
}

export default HomeRight