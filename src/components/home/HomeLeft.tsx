import React from 'react'
import {
    Container,
    HStack,
    Box,
    Text,
    Image,
    Link,
    VStack,
    Button,
    Badge,
  } from "@chakra-ui/react";
const HomeLeft = () => {
  return (
    <>
    <HStack flex="1" p="1em" align="center" justify="center">
              <Image
                src="https://firebasestorage.googleapis.com/v0/b/portfolio-595f6.appspot.com/o/Group%201%20(1).png?alt=media&token=f2665d5c-e877-4302-91f4-09e3df59020e"
                w="500px"
                h="600px"
                objectFit="contain"
                className="animate-img-jump"
              />
            </HStack></>
  )
}

export default HomeLeft