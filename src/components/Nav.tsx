import React from 'react'
import { Container ,HStack,Box,Text,Image,Link} from '@chakra-ui/react'

const Nav = () => {
  return (
    <Box w="80%" mx="auto" bg="#181E30" p="15px" justify="space between" borderBottom="2px solid #343A4E">
        <HStack w="full" align="center">
        <HStack flex="2">
            <Image src="https://firebasestorage.googleapis.com/v0/b/portfolio-595f6.appspot.com/o/Group%205%20(1).png?alt=media&token=ed42ac46-303e-4190-9409-05defa9a8be6" width="90px" height="60px"/>
        </HStack>
        <HStack color="white" fontWeight="600" spacing="40px"  flex="3" justify="flex-end" letterSpacing="0.7px">
            <Link><Text>Text</Text></Link>
            <Link><Text>About</Text></Link>
            <Link><Text>Blogs</Text></Link>
            <Link><Text>Work</Text></Link>
        </HStack>
        </HStack>
        </Box>
  )
}

export default Nav