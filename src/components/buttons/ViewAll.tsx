import React from 'react'

import {
  Container,
  HStack,
  Box,
  Text,
  Image,
  Link,
  VStack,
  Button,
  Badge,
  Icon,
} from "@chakra-ui/react";
import { ArrowForwardIcon } from "@chakra-ui/icons";
const ViewAll = () => {
  return (
    <>
     <Button
                      fontSize="0.7em"
                      px="1em"
                      py="0.5em"
                      fontWeight="600"
                      color="white"
                      bg="#edf2f76b"
                      color="white"
                      borderRadius="10px"
                      shadow="0px 0px 20px -5px #ffffff7a;"
                      _hover={{ bg: "#edf2f78c" }}
                      transition={"0.2s ease-in"}
                    >
                      <span>View All</span>
                      <ArrowForwardIcon mx="0.3em" fontSize="1.2em" />
                    </Button>
    </>
  )
}

export default ViewAll