import React from 'react'

import {
  Container,
  HStack,
  Box,
  Text,
  Image,
  Link,
  VStack,
  Button,
  Badge,
  Icon,
} from "@chakra-ui/react";
import { ArrowForwardIcon } from "@chakra-ui/icons";
const BtnMain = () => {
  return (
    <>
     <Button
                      fontSize="1.2em"
                      px="1.2em"
                      py="1.4em"
                      bg="#ffdb89"
                      color="#050505eb"
                      shadow="0px 0px 20px -5px #ffffff7a;"
                      _hover={{ bg: "#ffcd5d" }}
                      transition={"0.2s ease-in"}
                    >
                      <span>View Detail</span>
                    </Button>
    </>
  )
}

export default BtnMain