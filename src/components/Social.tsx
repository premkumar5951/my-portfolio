import React from "react";
import {
  Container,
  HStack,
  Box,
  Text,
  Image,
  Link,
  VStack,
  Button,
  Badge,
  Icon,
} from "@chakra-ui/react";

import { FaGithub } from "react-icons/fa";
import { FaGitlab } from "react-icons/fa";
import { FaLinkedin } from "react-icons/fa";
import { FaInstagram } from "react-icons/fa";
const Social = () => {
  return (
    <>
      <HStack flex="1">
        <Box
          textAlign="center"
          bg="white"
          width="2.5em"
          _hover={{"bg":"#ffdb89"}}
          cursor="pointer"
          height="2.5em"
          borderRadius="5em"
          display="flex"
          justifyContent="center"
          alignItems="center"
          shadow="0px 0px 20px -5px #625c5ca3;"
        >
          <Icon
            as={FaGithub}
            color="black"
            width="1.5rem"
            height="1.5rem"
          />
        </Box>
        <Box
          textAlign="center"
          bg="white"
          width="2.5em"
          _hover={{"bg":"#ffdb89"}}
          cursor="pointer"
          height="2.5em"
          borderRadius="5em"
          display="flex"
          justifyContent="center"
          alignItems="center"
          shadow="0px 0px 20px -5px #625c5ca3;"
        >
          <Icon
            as={FaGitlab}
            color="black"
            width="1.5rem"
            height="1.5rem"
          />
        </Box><Box
          textAlign="center"
          bg="white"
          width="2.5em"
          _hover={{"bg":"#ffdb89"}}
          cursor="pointer"
          height="2.5em"
          borderRadius="5em"
          display="flex"
          justifyContent="center"
          alignItems="center"
          shadow="0px 0px 20px -5px #625c5ca3;"
        >
          <Icon
            as={FaLinkedin}
            color="black"
            width="1.5rem"
            height="1.5rem"
          />
        </Box><Box
          textAlign="center"
          bg="white"
          width="2.5em"
          _hover={{"bg":"#ffdb89"}}
          cursor="pointer"
          height="2.5em"
          borderRadius="5em"
          display="flex"
          justifyContent="center"
          alignItems="center"
          shadow="0px 0px 20px -5px #625c5ca3;"
        >
          <Icon
            as={FaInstagram}
            color="black"
            width="1.5rem"
            height="1.5rem"
          />
        </Box>
     
      </HStack>
    </>
  );
};

export default Social;
