/** @type {import('tailwindcss').Config} */ 
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
    "./src/**/*.{js,ts,jsx,tsx}",
    './public/**/*.html',
  ],
  theme: {
    extend: {
      keyframes:{
        jump:{
          "0%":{
            transform: "translateY(0%)"
        },
        "25%":{
            transform: "translateY(1%)"
        },
        "50%":{
            transform: "translateY(2%)"
        },
        "75%":{
            transform: "translateY(1%)"
        },
        "100%":{
            transform: "translateY(0%)"
        },
        },
      },
      animation:{
        "img-jump":'jump 3s linear infinite alternate-reverse both'
      }
    },
  },
  plugins: [],
}